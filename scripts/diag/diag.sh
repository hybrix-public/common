#!/bin/sh
OLDPATH="$PATH"
WHEREAMI="`pwd`"
export PATH="$WHEREAMI/node_binaries/bin:$PATH"
NODEINST="`which node`"
UGLIFY=node_modules/uglify-es/bin/uglifyjs
CSSMIN=node_modules/cssmin/bin/cssmin

# $HYBRIXD/node/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"

echo "SCRIPTDIR: "$SCRIPTDIR


if [ "`uname`" = "Darwin" ]; then
    SYSTEM="darwin-x64"
elif [ "`uname -m`" = "i386" ] || [ "`uname -m`" = "i686" ] || [ "`uname -m`" = "x86_64" ] || [ "`uname -m`" = "aarch64" ]; then
    SYSTEM="linux-x64"
else
    echo "[!] Unknown architecture (or incomplete implementation)"
    exit 1;
fi

GREEN="\033[0;32m"
RED="\033[0;31m"
RESET="\033[0m"

checkGit(){
    branch_name=$(git symbolic-ref -q HEAD)
    branch_name=${branch_name##refs/heads/}
    branch_name=${branch_name:-HEAD}
    echo " [.] Current branch: $branch_name"

    git remote update > /dev/null
    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @{u})
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @{u} "$UPSTREAM")

    if [ "$LOCAL" = "$REMOTE" ]; then
        echo "  [.] Up-to-date"
    elif [ "$LOCAL" = "$BASE" ]; then
        echo "$RED  [.] Need to pull$RESET"
    elif [ "$REMOTE" = "$BASE" ]; then
        echo "$GREEN  [.] Need to push$RESET"
    else
        echo "$RED  [.] Diverged$RESET"
    fi
}

checkESLint(){
    if [ -z $(diff -q "$HYBRIXD/$1/.eslintrc.js" "$HYBRIXD/common/hooks/eslintrc.js") ]; then
        echo " [.] hybrixd/$1/.eslintrc.js is valid."
    else
        echo "$RED [!] hybrixd/$1/.eslintrc.js is modified .$RESET"
    fi

    if [ -z $(diff -q "$HYBRIXD/$1/.git/hooks/pre-push" "$HYBRIXD/common/hooks/pre-push") ]; then
        echo " [.] hybrixd/$1/pre-push is valid."
    else
        echo "$RED [!] hybrixd/$1/pre-push is modified .$RESET"
    fi

    if [ -z $(diff -q "$HYBRIXD/$1/.git/hooks/commit-msg" "$HYBRIXD/common/hooks/commit-msg") ]; then
        echo " [.] hybrixd/$1/commit-msg is valid."
    else
        echo "$RED [!] hybrixd/$1/commit-msg is modified .$RESET"
    fi
}

echo "[.] Validate hybrixd/node."
if [ -d "$HYBRIXD/node" ]; then
    echo " [.] hybrixd/node found."

    if [ -L "$HYBRIXD/node/common" ]; then
        echo " [.] hybrixd/node/common found."
        if [ "$(readlink $HYBRIXD/node/common)" = "$HYBRIXD/common" ]; then
            echo " [.] hybrixd/node/common linked correctly."
        else
            echo "$RED [!] hybrixd/node/common linked incorrectly."
            echo "     Expected: $HYBRIXD/common"
            echo "     Found:    $(readlink $HYBRIXD/node/common)$RESET"
        fi
    else
        echo " [!] hybrixd/node/common not linked."
    fi

    if [ -L "$HYBRIXD/node/node_binaries" ]; then
        echo " [.] hybrixd/node/node_binaries found."
        if [ "$(readlink $HYBRIXD/node/node_binaries)" = "$HYBRIXD/nodejs/$SYSTEM" ]; then
            echo " [.] hybrixd/node/node_binaries linked correctly."
        else
            echo "$RED [!] hybrixd/node/node_binaries linked incorrectly."
            echo "     Expected: $HYBRIXD/nodejs/$SYSTEM"
            echo "     Found:    $(readlink $HYBRIXD/node/node_binaries)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/node/node_binaries not linked.$RESET"
    fi


    #TODO interface dist
    #TODO determinstic dist
    #TODO web-wallet dist

    #TODO check if up to date
    # DEPRECATD: checkESLint "node"

    cd "$HYBRIXD/node/"
    checkGit


else
    echo " [!] hybrixd/node not found."
fi


echo "[.] Validate hybrixd/web-wallet."
if [ -d "$HYBRIXD/web-wallet" ]; then
    echo " [.] hybrixd/web-wallet found."

    if [ -L "$HYBRIXD/web-wallet/common" ]; then
        echo " [.] hybrixd/web-wallet/common found."
        if [ "$(readlink $HYBRIXD/web-wallet/common)" = "$HYBRIXD/common" ]; then
            echo " [.] hybrixd/web-wallet/common linked correctly."
        else
            echo "$RED [!] hybrixd/web-wallet/common linked incorrectly."
            echo "     Expected: $HYBRIXD/common"
            echo "     Found:    $(readlink $HYBRIXD/web-wallet/common)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/web-wallet/common not linked (Known Issue).$RESET"
    fi

    if [ -L "$HYBRIXD/web-wallet/interface" ]; then
        echo " [.] hybrixd/web-wallet/interface found."
        if [ "$(readlink $HYBRIXD/web-wallet/interface)" = "$HYBRIXD/hybrix-lib/dist" ]; then
            echo " [.] hybrixd/web-wallet/interface linked correctly."
        else
            echo "$RED [!] hybrixd/web-wallet/interface linked incorrectly."
            echo "     Expected: $HYBRIXD/hybrix-lib/dist"
            echo "     Found:    $(readlink $HYBRIXD/web-wallet/interface)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/web-wallet/interface not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/web-wallet/node_binaries" ]; then
        echo " [.] hybrixd/web-wallet/node_binaries found."
        if [ "$(readlink $HYBRIXD/web-wallet/node_binaries)" = "$HYBRIXD/nodejs/$SYSTEM" ]; then
            echo " [.] hybrixd/web-wallet/node_binaries linked correctly."
        else
            echo "$RED [!] hybrixd/web-wallet/node_binaries linked incorrectly."
            echo "     Expected: $HYBRIXD/nodejs/$SYSTEM"
            echo "     Found:    $(readlink $HYBRIXD/web-wallet/node_binaries)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/web-wallet/node_binaries not linked (Known Issue).$RESET"
    fi

    cd "$HYBRIXD/web-wallet/"
    checkGit
    if [ "$(sh $HYBRIXD/web-wallet/scripts/npm/check.sh)" = "UP TO DATE" ]; then
        echo " [.] Distributables up to date."
    else
        echo "$RED [!] Source files modified. Distributables not up te date.$RESET"

    fi
    checkESLint "web-wallet"

else
    echo " [.] hybrixd/web-wallet not found."
fi


echo "[.] Validate hybrixd/tui-wallet."
if [ -d "$HYBRIXD/tui-wallet" ]; then
    echo " [.] hybrixd/tui-wallet found."

    if [ -L "$HYBRIXD/tui-wallet/common" ]; then
        echo " [.] hybrixd/tui-wallet/common found."
        if [ "$(readlink $HYBRIXD/tui-wallet/common)" = "$HYBRIXD/common" ]; then
            echo " [.] hybrixd/tui-wallet/common linked correctly."
        else
            echo "$RED [!] hybrixd/tui-wallet/common linked incorrectly."
            echo "     Expected: $HYBRIXD/common"
            echo "     Found:    $(readlink $HYBRIXD/tui-wallet/common)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/tui-wallet/common not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/tui-wallet/interface" ]; then
        echo " [.] hybrixd/tui-wallet/interface found."
        if [ "$(readlink $HYBRIXD/tui-wallet/interface)" = "$HYBRIXD/hybrix-lib/dist" ]; then
            echo " [.] hybrixd/tui-wallet/interface linked correctly."
        else
            echo "$RED [!] hybrixd/tui-wallet/interface linked incorrectly."
            echo "     Expected: $HYBRIXD/hybrix-lib/dist"
            echo "     Found:    $(readlink $HYBRIXD/tui-wallet/interface)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/tui-wallet/interface not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/tui-wallet/node_binaries" ]; then
        echo " [.] hybrixd/tui-wallet/node_binaries found."
        if [ "$(readlink $HYBRIXD/tui-wallet/node_binaries)" = "$HYBRIXD/nodejs/$SYSTEM" ]; then
            echo " [.] hybrixd/tui-wallet/node_binaries linked correctly"
        else
            echo "$RED [!] hybrixd/tui-wallet/node_binaries linked incorrectly."
            echo "     Expected: $HYBRIXD/nodejs/$SYSTEM"
            echo "     Found:    $(readlink $HYBRIXD/tui-wallet/node_binaries)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/tui-wallet/node_binaries not linked.$RESET"
    fi
    checkESLint "tui-wallet"

    cd "$HYBRIXD/tui-wallet/"
    checkGit

else
    echo " [.] hybrixd/tui-wallet not found."
fi

echo "[.] Validate hybrixd/cli-wallet."
if [ -d "$HYBRIXD/cli-wallet" ]; then
    echo " [.] hybrixd/cli-wallet found."

    if [ -L "$HYBRIXD/cli-wallet/common" ]; then
        echo " [.] hybrixd/cli-wallet/common found."
        if [ "$(readlink $HYBRIXD/cli-wallet/common)" = "$HYBRIXD/common" ]; then
            echo " [.] hybrixd/cli-wallet/common linked correctly."
        else
            echo "$RED [!] hybrixd/cli-wallet/common linked incorrectly."
            echo "     Expected: $HYBRIXD/common"
            echo "     Found:    $(readlink $HYBRIXD/cli-wallet/common)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/cli-wallet/common not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/cli-wallet/interface" ]; then
        echo " [.] hybrixd/cli-wallet/interface found."
        if [ "$(readlink $HYBRIXD/cli-wallet/interface)" = "$HYBRIXD/hybrix-lib/dist" ]; then
            echo " [.] hybrixd/cli-wallet/interface linked correctly."
        else
            echo "$RED [!] hybrixd/cli-wallet/interface linked incorrectly."
            echo "     Expected: $HYBRIXD/hybrix-lib/dist"
            echo "     Found:    $(readlink $HYBRIXD/cli-wallet/interface)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/cli-wallet/interface not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/cli-wallet/node_binaries" ]; then
        echo " [.] hybrixd/cli-wallet/node_binaries found."
        if [ "$(readlink $HYBRIXD/cli-wallet/node_binaries)" = "$HYBRIXD/nodejs/$SYSTEM" ]; then
            echo " [.] hybrixd/cli-wallet/node_binaries linked correctly."
        else
            echo "$RED [!] hybrixd/cli-wallet/node_binaries linked incorrectly."
            echo "     Expected: $HYBRIXD/nodejs/$SYSTEM"
            echo "     Found:    $(readlink $HYBRIXD/cli-wallet/node_binaries)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/cli-wallet/node_binaries not linked.$RESET"
    fi

    checkESLint "cli-wallet"

    cd "$HYBRIXD/cli-wallet/"
    checkGit

else
    echo " [.] hybrixd/cli-wallet not found."
fi


echo "[.] Validate hybrixd/deterministic."
if [ -d "$HYBRIXD/deterministic" ]; then
    echo " [.] hybrixd/deterministic found."

    if [ -L "$HYBRIXD/deterministic/common" ]; then
        echo " [.] hybrixd/deterministic/common found."
        if [ "$(readlink $HYBRIXD/deterministic/common)" = "$HYBRIXD/common" ]; then
            echo " [.] hybrixd/deterministic/common linked correctly."
        else
            echo "$RED [!] hybrixd/deterministic/common linked incorrectly."
            echo "     Expected: $HYBRIXD/common"
            echo "     Found:    $(readlink $HYBRIXD/deterministic/common)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/deterministic/common not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/deterministic/interface" ]; then
        echo " [.] hybrixd/deterministic/interface found."
        if [ "$(readlink $HYBRIXD/deterministic/interface)" = "$HYBRIXD/hybrix-lib/dist" ]; then
            echo " [.] hybrixd/deterministic/interface linked correctly."
        else
            echo "$RED [!] hybrixd/deterministic/interface linked incorrectly."
            echo "     Expected: $HYBRIXD/hybrix-lib/dist"
            echo "     Found:    $(readlink $HYBRIXD/deterministic/interface)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/deterministic/interface not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/deterministic/node_binaries" ]; then
        echo " [.] hybrixd/deterministic/node_binaries found."
        if [ "$(readlink $HYBRIXD/deterministic/node_binaries)" = "$HYBRIXD/nodejs/$SYSTEM" ]; then
            echo " [.] hybrixd/deterministic/node_binaries linked correctly."
        else
            echo "$RED [!] hybrixd/deterministic/node_binaries linked incorrectly."
            echo "     Expected: $HYBRIXD/nodejs/$SYSTEM"
            echo "     Found:    $(readlink $HYBRIXD/deterministic/node_binaries)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/deterministic/node_binaries not linked.$RESET"
    fi

    checkESLint "deterministic"

    cd "$HYBRIXD/deterministic/"
    checkGit

    if [ "$(sh $HYBRIXD/deterministic/scripts/npm/check.sh)" = "UP TO DATE" ]; then
        echo " [.] Distributables up to date."
    else
        echo "$RED [!] Source files modified. Distributables not up te date.$RESET"

    fi

else
    echo " [.] hybrixd/deterministic not found."
fi


echo "[.] Validate hybrixd/hybrix-lib"
if [ -d "$HYBRIXD/hybrix-lib" ]; then
    echo " [.] hybrixd/hybrix-lib found."

    if [ -L "$HYBRIXD/hybrix-lib/common" ]; then
        echo " [.] hybrixd/hybrix-lib/common found."
        if [ "$(readlink $HYBRIXD/hybrix-lib/common)" = "$HYBRIXD/common" ]; then
            echo " [.] hybrixd/hybrix-lib/common linked correctly."
        else
            echo "$RED [!] hybrixd/hybrix-lib/common linked incorrectly."
            echo "     Expected: $HYBRIXD/common"
            echo "     Found:    $(readlink $HYBRIXD/hybrix-lib/common)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/hybrix-lib/common not linked.$RESET"
    fi

    if [ -L "$HYBRIXD/hybrix-lib/node_binaries" ]; then
        echo " [.] hybrixd/hybrix-lib/node_binaries found."
        if [ "$(readlink $HYBRIXD/hybrix-lib/node_binaries)" = "$HYBRIXD/nodejs/$SYSTEM" ]; then
            echo " [.] hybrixd/hybrix-lib/node_binaries linked correctly."
        else
            echo "$RED [!] hybrixd/hybrix-lib/node_binaries linked incorrectly."
            echo "     Expected: $HYBRIXD/nodejs/$SYSTEM"
            echo "     Found:    $(readlink $HYBRIXD/hybrix-lib/node_binaries)$RESET"
        fi
    else
        echo "$RED [!] hybrixd/hybrix-lib/node_binaries not linked.$RESET"
    fi

    checkESLint "interface"

    cd "$HYBRIXD/hybrix-lib/"
    checkGit

    if [ "$(sh $HYBRIXD/hybrix-lib/scripts/npm/check.sh)" = "UP TO DATE" ]; then
        echo " [.] Distributables up to date."
    else
        echo "$RED [!] Source files modified. Distributables not up te date.$RESET"

    fi


else
    echo " [.] hybrixd/hybrix-lib not found."
fi

echo "[.] Validate hybrixd/common."
if [ -d "$HYBRIXD/common" ]; then
    echo " [.] hybrixd/common found."

    checkESLint "common"

    cd "$HYBRIXD/common"
    checkGit

    #TODO check if up to date
else
    echo " [!] hybrixd/common not found."
fi

export PATH="$OLDPATH"
cd "$WHEREAMI"
