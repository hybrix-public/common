const jsbinaryType = require('js-binary').Type
const sha256 = require('js-sha256');

const SECONDS_IN_A_DAY = 86400;

const jsbinaryStorageSchema = function () {
  return new jsbinaryType({
    hash: 'Buffer',
    size: 'uint',
    publicKey: 'Buffer',
    signature: 'Buffer',
    time: {
      create: 'date',
      read: 'date',
      update: 'date',
      expire: 'date'
    },
    hops: 'uint'
  });
};

const sha256Buffer = function (data) {
  return data===undefined||data===null?'':Buffer.from(sha256.sha256(data),'hex');
};

const createHexBuffer = function (data) {
  return Buffer.from(data===undefined||data===null?'':data,'hex');
};

const formatMetaResponse = function (meta,action) {
  let result = {};
  // add action
  if (action) result.action = action;
  // decode hashes from buffers
  result.id = meta.id;
  result.hash = meta.hash.toString('hex');
  result.size = meta.size?meta.size:0;
  result.hops = meta.hops?meta.hops:0;
  result.publicKey = meta.publicKey.toString('hex');
  result.signature = meta.signature.toString('hex');
  // translate timestamps to milliseconds-epoch or null if zero
  result.time = {
    create : (meta.time.create.valueOf()?meta.time.create.valueOf():null),
    read : (meta.time.read.valueOf()?meta.time.read.valueOf():null),
    update : (meta.time.update.valueOf()?meta.time.update.valueOf():null),
    expire : (meta.time.expire.valueOf()?meta.time.expire.valueOf():null)
  }
  return result;
};

const emptyMeta = function () {
  const zerodate = new Date(0);
  const emptyBuffer = createHexBuffer('');
  return {
    hash: emptyBuffer,
    size: 0,
    hops: 0,
    publicKey: emptyBuffer,
    signature: emptyBuffer,
    time: {
      create: zerodate,
      read: zerodate,
      update: zerodate,
      expire: zerodate
    }
  };
};

const calculateExpiry = function (dateStart, fileSize, STORAGECONF) {
  let storageDays = STORAGECONF.storage.storageLimit/fileSize;
  if (storageDays < STORAGECONF.storage.minStorageDays) storageDays = STORAGECONF.storage.minStorageDays
  else if (storageDays > STORAGECONF.storage.maxStorageDays) storageDays = STORAGECONF.storage.maxStorageDays;
  const expire = Math.round(dateStart.valueOf() + (storageDays * SECONDS_IN_A_DAY * 1000));
  return expire;
}

exports.jsbinaryStorage = jsbinaryStorageSchema;
exports.sha256Buffer = sha256Buffer;
exports.createHexBuffer = createHexBuffer;
exports.emptyMeta = emptyMeta;
exports.formatMetaResponse = formatMetaResponse;
exports.calculateExpiry = calculateExpiry;
