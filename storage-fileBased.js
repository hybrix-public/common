/* storage-fileBased.js library for hybrix - (C) 2022 Joachim de Koning
 *
 * Ephemeral local file-based storage with metadata functionality,
 * proof-of-work retainment policies, and signature registeredOwnership.
 */
const fs = require('fs');
const du = require('du');
const glob = require('glob');
const util = require('util');
const baseCode = require('./basecode');
const storageCommon = require('./storage-common');
// NOTE: Checks if hybrixd module is running this, and adds ../ if so.
//       This stores files starting from the root, and is a hack which could be done better.
let STORAGECONF = require('./storage-fileBased-defaults.json');
if (typeof global !== 'undefined' && global.hasOwnProperty('hybrixd')) {
  STORAGECONF.storage.storagePath = '../'+STORAGECONF.storage.storagePath;
}
const STORAGEPATH = require('path').normalize(process.cwd() + '/' + STORAGECONF.storage.storagePath);
const DOTMETA = '.meta';
const jsbinaryStorage = storageCommon.jsbinaryStorage(); // meta storage schema
const sha256Buffer = storageCommon.sha256Buffer;
const createHexBuffer = storageCommon.createHexBuffer;
const formatMetaResponse = storageCommon.formatMetaResponse;

const fsExists = util.promisify(fs.exists);
const fsStat = util.promisify(fs.stat);
const fsMkdir = util.promisify(fs.mkdir);
const fsReadFile = util.promisify(fs.readFile);
const fsWriteFile = util.promisify(fs.writeFile);
const fsUnlink = util.promisify(fs.unlink);

const fold = function (key) {
  return `/${key.substr(0, STORAGECONF.storage.substorageIdLength)}/`;
};

const makeDir = async function (dirname) {
  const dirExists = await fsExists(dirname);
  if (!dirExists) {
    try {
      await fsMkdir(dirname, { recursive: true });
    } catch(e) {
      console.error('filebased storage: Cannot make new directory!');
    }
    return true;
  }
  return false;
};

const seek = async function ({key}, dataCallback, errorCallback) {
  if (typeof key === 'string') {
    const filePath = STORAGEPATH + fold(key) + key;
    if (await fsExists(filePath)) {
      dataCallback(true);
    } else {
      dataCallback(false);
    }
  } else errorCallback('filebased storage: Seek expects key input to be a string');
  return;
};

// if .meta file does not exist, return empty object
// this async becomes a promise, so simply return value (or Throw error, but this time we don't need that)
async function getMetaOrEmpty(filePath) {
  let meta = storageCommon.emptyMeta();
  try {
    const fileExists = await fsExists(filePath + DOTMETA);
    if (fileExists) {
      try {
        const metadata = await fsReadFile(filePath + DOTMETA);
        meta = jsbinaryStorage.decode(metadata);
      } catch (e) {
        // DEBUG: console.warn('filebased meta: Data could not be read for '+filePath);
      }
    }
  } catch(e) {
    console.warn('filebased meta: Meta file does not exist '+filePath);
  }
  return meta;
};

const load = async function (data, dataCallback, errorCallback) {
  const filePath = STORAGEPATH + fold(data.key) + data.key;
  const fileExists = await fsExists(filePath);
  if (fileExists) {
    const meta = await getMetaOrEmpty(filePath);
    const buffer = await fsReadFile(filePath);
    if (Object.keys(meta.publicKey).length>0 && Object.keys(meta.signature).length>0) {
      const publicKeyNACL = meta.publicKey; // when data is owned, loaded data must be properly signed!
      const signatureNACL = meta.signature;
      const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', buffer) );
      const verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
      if (!verified) {
        errorCallback('filebased load: Data corrupted!');
        return;
      }
    }
    let result;
    switch (data.type) {
      case 'buffer':
        result = buffer;
        break;
      case 'path':
        result = STORAGECONF.storage.storagePath + fold(data.key) + data.key; // use only relative path here!
        break;
      case 'string':
      default:
        result = buffer.toString();
    }
    const now = new Date();
    meta.time.read = now; // update metadata with last time read
    await fsWriteFile(filePath + DOTMETA, jsbinaryStorage.encode(meta));
    dataCallback(result);
    return;
  } else {
    errorCallback('filebased load: Data not found!');
    return;
  }
};

const createFile = async (key, value, publicKey, signature, dataCallback, errorCallback) => {
  const filePath = STORAGEPATH + fold(key) + key;
  const hash = sha256Buffer(value);
  const isHEX = /[0-9A-Fa-f]/g;
  let publicKeyNACL, signatureNACL;
  if (publicKey) {
    if (!signature) {
      errorCallback('filebased save: Create data storage expected both publicKey and signature!');
      return;
    }
    if (publicKey.match(isHEX)) publicKeyNACL = nacl.from_hex(publicKey); else {
      errorCallback('filebased save: Create data storage expected public key to be in hexadecimal format!');
      return;
    }
  }
  if (signature) {
    if (!publicKeyNACL) {
      errorCallback('filebased save: Create data storage expected both publicKey and signature!');
      return;
    }
    if (signature.match(isHEX)) signatureNACL = nacl.from_hex(signature); else {
      errorCallback('filebased save: Create data storage expected signature to be in hexadecimal format!');
      return;
    }
  }
  if (typeof publicKeyNACL !== 'undefined' && typeof signatureNACL !== 'undefined') {
    const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', value) );
    const verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
    if (!verified) {
      errorCallback('filebased save: Create data storage rejected because of bad publicKey and/or signature!');
      return;
    }
  }
  const now = new Date();
  const size = value.length;
  const expire = new Date(storageCommon.calculateExpiry(now,size,STORAGECONF));
  const zerodate = new Date(0);
  const meta = {
    hash,
    size,
    publicKey: createHexBuffer(publicKey),
    signature: createHexBuffer(signature),
    time: {
      create: now,
      read: zerodate,
      update: now,
      expire: expire
    },
    hops: 0
  };
  try {
    await fsWriteFile(filePath, value);
    await fsWriteFile(filePath + DOTMETA, jsbinaryStorage.encode(meta));
  } catch (e) {
    errorCallback('filebased save: Could not create storage file -> '+e);
    return;
  }
  dataCallback(formatMetaResponse(meta,'create'));
  return;
};

const updateFile = async (key, value, publicKey, signature, dataCallback, errorCallback) => {
  const filePath = STORAGEPATH + fold(key) + key;
  const meta = await getMetaOrEmpty(filePath);
  const registeredOwner = meta.publicKey?meta.publicKey.toString('hex'):false; // check registeredOwnership
  if (publicKey || registeredOwner) {
    let verified = true;
    const isHEX = /[0-9A-Fa-f]/g;
    let publicKeyNACL;
    if (registeredOwner && publicKey === registeredOwner) {
      if (publicKey.match(isHEX)) {
        publicKeyNACL = nacl.from_hex(publicKey);
        if (signature) {
          if (signature.match(isHEX)) {
            const signatureNACL = nacl.from_hex(signature);
            const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', value) );
            verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
          } else {
            errorCallback('filebased save: Expected signature to be in hexadecimal format!');
            return;
          }
        } else {
          errorCallback('filebased save: Expected signature to be added to storage object!');
          return;
        }
      } else {
        errorCallback('filebased save: Expected public key to be in hexadecimal format!');
        return;
      }
    } else if (registeredOwner) {
      errorCallback('filebased save: Update data storage rejected because public key missing or mismatch!');
      return;
    }
    if (!verified) {
      errorCallback('filebased save: Create data storage rejected because of bad publicKey and/or signature!');
      return;
    }
  }
  const hash = sha256Buffer(value);
  if (meta.hash && meta.hash.toString('hex') === hash.toString('hex') && publicKey === registeredOwner) {
    dataCallback(formatMetaResponse(meta,'none'));
    return;
  }
  const now = new Date();
  const size = value.length;
  const expire = new Date(storageCommon.calculateExpiry(now,size,STORAGECONF));
  const timeCreate = !isNaN(meta.time.create)&&meta.time.create>0?meta.time.create:now;
  const timeRead = meta.time.read;
  meta.time = {
    create: timeCreate,
    read: timeRead,
    update: now,
    expire: expire
  }
  meta.hash = hash;
  meta.size = size;
  meta.publicKey = createHexBuffer(publicKey);
  meta.signature = createHexBuffer(signature);
  try {
    await fsWriteFile(filePath, value);
    await fsWriteFile(filePath + DOTMETA, jsbinaryStorage.encode(meta));
  } catch (e) {
    errorCallback('filebased save: Could not update storage file! '+e);
    return;
  }
  dataCallback(formatMetaResponse(meta,'update'));
  return;
};

const save = async (data, dataCallback, errorCallback) => {
  if (!data.sizeOverride && data.value.length > STORAGECONF.storage.storageLimit) {
    errorCallback('filebased save: Storage limit is ' + STORAGECONF.storage.storageLimit + ' bytes!');
  } else {
    await makeDir(STORAGEPATH + fold(data.key));
    const filePath = STORAGEPATH + fold(data.key) + data.key;
    let fileExists;
    try {
      fileExists = await fsExists(filePath);
    } catch(e) {
      errorCallback('filebased save: Error on check file exists!');
      return;
    }
    if (fileExists) {
      updateFile(data.key, data.value, data.publicKey, data.signature, dataCallback, errorCallback);
    } else {
      createFile(data.key, data.value, data.publicKey, data.signature, dataCallback, errorCallback);
    }
  }
  return;  
};

const list = async ({key}, dataCallback, errorCallback) => {
  const padLength = String(STORAGECONF.storage.substorageIdLength - 1).length;
  if (key.length < padLength) {
    errorCallback('filebased save: Specify a search string of ' + (padLength + 1) + ' or more characters!');
  } else {
    glob(key, {cwd: STORAGEPATH, matchBase:true, nodir: true}, function (err, files) {
      if (err) {
        errorCallback('filebased save: Error reading storage list!');
      } else {
        const result = [];
        const prefixLength = STORAGECONF.storage.substorageIdLength+1;
        for (let i = 0; i < files.length; i++) {
          if (files[i].substr(-DOTMETA.length) !== DOTMETA) {
            result.push(files[i].substr(prefixLength));
          }
        }
        dataCallback(result);
      }
    });
  }
  return;  
};

const burn = async (data, dataCallback, errorCallback) => {
  const filePath = STORAGEPATH + fold(data.key) + data.key;
  const meta = await getMetaOrEmpty(filePath);
  const publicKey = data.publicKey;
  const signature = data.signature;
  const registeredOwner = meta.publicKey?meta.publicKey.toString('hex'):false; // check registeredOwnership
  if (publicKey || registeredOwner) {
    let verified = true;
    const isHEX = /[0-9A-Fa-f]/g;
    let publicKeyNACL;
    if (registeredOwner && publicKey === registeredOwner) {
      if (publicKey.match(isHEX)) {
        publicKeyNACL = nacl.from_hex(publicKey);
        if (signature) {
          if (signature.match(isHEX)) {
            const signatureNACL = nacl.from_hex(signature);
            const bufferNACL = nacl.from_hex( baseCode.recode('utf-8', 'hex', data.key) );
            verified = nacl.crypto_sign_verify_detached(signatureNACL, bufferNACL, publicKeyNACL);
          } else {
            errorCallback('filebased burn: Expected signature to be in hexadecimal format!');
            return;
          }
        } else {
          errorCallback('filebased burn: Expected signature to be added to storage object!');
          return;
        }
      } else {
        errorCallback('filebased burn: Expected public key to be in hexadecimal format!');
        return;
      }
    } else if (registeredOwner) {
      errorCallback('filebased burn: Storage burn data rejected because public key missing or mismatch!');
      return;
    }
    if (!verified) {
      errorCallback('filebased burn: Storage burn data rejected because of bad publicKey and/or signature!');
      return;
    }
  }
  let fileExists, fileExistsMeta;
  try {
    fileExists = await fsExists(filePath);
  } catch(e) {
    errorCallback('filebased burn: Error on check file exists!');
    return;
  }
  if (fileExists) {
    try {
      await fsUnlink(filePath);
    } catch(e) {
      errorCallback('filebased burn: Could not delete ' + fold(data.key) + data.key);
      return;
    }
  }
  try {
    fileExistsMeta = await fsExists(filePath + DOTMETA);
  } catch(e) {
    errorCallback('filebased burn: Error on check meta file exists!');
    return;
  }
  if (fileExistsMeta) {
    try {
      await fsUnlink(filePath + DOTMETA);
    } catch(e) {
      errorCallback('filebased burn: Could not delete ' + fold(data.key) + data.key + DOTMETA);
      return;
    }
  }
  if (fileExists && fileExistsMeta) {
    dataCallback(formatMetaResponse(meta,'delete'));
  } else if (!fileExists) {
    dataCallback('filebased burn: Key does not exist!');
  } else {
    dataCallback('filebased burn: Key burned, but metadata did not exist!');
  }
  return;
};

const getMeta = async ({key}, dataCallback, errorCallback) => {
  const filePath = STORAGEPATH + fold(key) + key;
  let fileExists
  try {
    fileExists = await fsExists(filePath + DOTMETA)
  } catch(e) {
    return errorCallback('filebased meta: Error on check file exists!');
  }
  let data
  try {
    if (fileExists) {
      const meta = await getMetaOrEmpty(filePath);
      data = formatMetaResponse({id:key, ...meta},false);
    } else {
      // DEPRECATED: errorCallback('filebased meta: Data not found!');
      data = null
    }
  } catch(e) {
    return errorCallback('filebased meta: Metadata error -> '+e);
  }
  return dataCallback(data);
};

const setHops = async ({key,value}, dataCallback, errorCallback) => {
  const filePath = STORAGEPATH + fold(key) + key;
  let fileExists
  try {
    fileExists = await fsExists(filePath + DOTMETA);
  } catch(e) {
    errorCallback('filebased hops: Error on check file exists!');
    return;
  }
  let data;
  try {
    if (fileExists) {
      const meta = await getMetaOrEmpty(filePath);
      meta.hops = !isNaN(value) && value >= 0 ? parseInt(value) : 0;
      await fsWriteFile(filePath + DOTMETA, jsbinaryStorage.encode(meta));
      data = formatMetaResponse({id:key, ...meta},false);
      dataCallback(data);
    } else {
      errorCallback('filebased hops: Data not found!');
    }
  } catch(e) {
    errorCallback('filebased hops: Metadata error -> '+e);
  }
  return;
};


// TODO: console.logs to DEBUG!
/*
const getFilesizeInBytes = async function (filename) {
  // TODO: try/catch!
  const stats = await fsStat(filename);
  const fileSizeInBytes = stats.size;
  return fileSizeInBytes;
};

const autoClean = function () {
  if (!fs.existsSync(STORAGEPATH)) {
    console.log('creating storage directory');
    fs.mkdirSync(STORAGEPATH, { recursive: true });
    return; // if path did not exist it's already cleaned
  }
  console.log('auto-clean scan');

  const now = Date.now();
  du(storagePath, function (e, storageLimit) {
    console.log('size is ' + storageLimit + ' bytes');
    fs.writeFileSync(STORAGEPATH + '/size', storageLimit); // store size for /size call

    if (storageLimit > STORAGECONF.storage.storageLimit) {
      console.log('size maximum reached, cleaning...');

      fs.readdir(STORAGEPATH, (e, directories) => {
        // scan storage directories
        directories.forEach((fold, dirindex, dirarray) => {
          if (fs.statSync(STORAGEPATH + fold).isDirectory()) {
            // DEBUG: console.log(['info', 'storage'], 'found directory ' + storepath + fold);
            fs.readdir(STORAGEPATH + fold, (e, files) => {
              files.forEach((storekey, fileindex, filearray) => {
                if (storekey.substr(-5) === DOTMETA) {
                  let fileelement = `${STORAGEPATH}/${fold}/${storekey}`;
                  // DEBUG: console.log(['info', 'storage'], 'test on storage ' + fileelement);
                  if (fs.existsSync(fileelement)) {
                    let meta = JSON.parse(String(fs.readFileSync(fileelement)));

                    let minstoragetime = meta.time + STORAGECONF.storage.minStorageDays * SECONDS_IN_A_DAY;
                    let maxstoragetime = meta.time + STORAGECONF.storage.maxStorageDays * SECONDS_IN_A_DAY;
                    let powstoragetime = meta.expire;
                    const expire = meta.expire === null ? minstoragetime : (maxstoragetime > powstoragetime ? maxstoragetime : powstoragetime);

                    if (storageLimit > STORAGECONF.storage.storageLimit && expire < now) {
                      let dataelement = fileelement.substr(0, fileelement.length - 5);
                      try {
                        // get filesize and subtract that from storageLimit
                        let deleteSize = getFilesizeInBytes(fileelement) + getFilesizeInBytes(dataelement);
                        storageLimit = storageLimit - deleteSize;
                        // delete the file and metadata
                        fs.unlinkSync(dataelement);
                        fs.unlinkSync(fileelement);
                        console.log('purged stale storage element' + dataelement);
                      } catch (e) {
                        console.log('failed to purge stale storage ' + dataelement);
                      }
                    }
                  }
                }
              });
            });
          }
        });
      });
    } else {
      console.log('no cleaning necessary');
    }
  });
};
*/

exports.load = load;
exports.save = save;
exports.burn = burn;
exports.meta = getMeta;
exports.seek = seek;
exports.list = list;
exports.setHops = setHops;
//exports.autoClean = autoClean;
