/* cryptography.js library for hyde - (C) 2022 Joachim de Koning
 *
 * Cryptographic methods for clean and painless usage of Elliptic Curve cryptography.
 *
 * In public-key cryptography, Edwards-curve Digital Signature Algorithm (EdDSA)
 * is a digital signature scheme using a variant of Schnorr signature based on
 * twisted Edwards curves.[1] It is designed to be faster than existing digital
 * signature schemes without sacrificing security. It was developed by a team
 * including Daniel J. Bernstein, Niels Duif, Tanja Lange, Peter Schwabe, and
 * Bo-Yin Yang. The reference implementation is public domain software.
 */

const EdDSA = require('elliptic').eddsa;
const ec = new EdDSA('ed25519'); // we use elliptic curve ED5519

// internal function
const generateKeyPair = (secret) => {
  if (secret && secret.length < 64) {
    return errorCallback('Need at least 64 bytes of entropy!');
  }
  return ec.keyFromSecret(secret); // hex string, array or Buffer ();
}

// { secret }
const getPublicKey = (data,dataCallback,errorCallback) => {
  const keyPair = generateKeyPair(data.secret);
  const publicKey = keyPair.getPublic('hex');
  return dataCallback(publicKey);
}

// { data,secret } => publicKey
const createSignature = (data,dataCallback,errorCallback) => {
  const keyPair = generateKeyPair(data.secret);
  const signature = keyPair.sign(data.data).toHex().toLowerCase();
  return dataCallback(signature);
}

// { data,secret } => { publicKey,signature }
const getPublicKeyAndCreateSignature = (data,dataCallback,errorCallback) => {
  const keyPair = generateKeyPair(data.secret);
  const publicKey = keyPair.getPublic('hex');
  const signature = keyPair.sign(data.data).toHex().toLowerCase();
  return dataCallback({publicKey,signature});
}

// { hash,signature,publicKey } => boolean
const validateSignature = (data) => {
  if (!data.data || !data.data.length === 0 || !data.signature || data.signature.length === 0 || !data.publicKey || data.publicKey.length === 0) {
    return false;
  } else {
    const publicKey = ec.keyFromPublic(data.publicKey, 'hex'); // import public key
    return publicKey.verify(data.data, data.signature);
  }
}

exports.getPublicKey = getPublicKey;
exports.createSignature = createSignature;
exports.getPublicKeyAndCreateSignature = getPublicKeyAndCreateSignature;
exports.validateSignature = validateSignature;
